﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ClientLauncher
{
	// Token: 0x02000029 RID: 41
	internal class FolderHash
	{
		// Token: 0x0600015B RID: 347 RVA: 0x0000674C File Offset: 0x0000494C
		public static string GetHashOfDir(string path)
		{
			return FolderHash.SHA1HashString(FolderHash.GetFullHashOfDir(path)).ToUpper();
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00006760 File Offset: 0x00004960
		private static string GetFullHashOfDir(string path)
		{
			string text = "";
			if (!Utility.DirectoryExists(path))
			{
				return "";
			}
			string[] array = Directory.GetDirectories(path);
			for (int i = 0; i < array.Length; i++)
			{
				string path2 = array[i];
				text += FolderHash.GetFullHashOfDir(path2);
			}
			array = Directory.GetFiles(path);
			for (int i = 0; i < array.Length; i++)
			{
				string f = array[i];
				text += FileHash.sha1(f);
			}
			return text;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x000067D0 File Offset: 0x000049D0
		private static string SHA1HashString(string s)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(s);
			return FolderHash.HexStringFromBytes(SHA1.Create().ComputeHash(bytes));
		}

		// Token: 0x0600015E RID: 350 RVA: 0x000067FC File Offset: 0x000049FC
		private static string HexStringFromBytes(byte[] bytes)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++)
			{
				byte b = bytes[i];
				string value = b.ToString("x2");
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}
	}
}
