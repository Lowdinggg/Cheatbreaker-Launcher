﻿using System;
using System.IO;

namespace ClientLauncher
{
	// Token: 0x0200002A RID: 42
	internal class VersionManager
	{
		// Token: 0x06000160 RID: 352 RVA: 0x00006840 File Offset: 0x00004A40
		public VersionManager()
		{
			this._versionDir = CheatBreakerGlobals.GetVersionDir("").Replace("\\\\", "\\");
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00006868 File Offset: 0x00004A68
		public void GetForVersion(Downloader downloader, string version)
		{
			string text = string.Concat(new string[]
			{
				this._versionDir,
				"/",
				version,
				"/",
				version,
				".jar"
			});
			if (!Utility.FileExists(text))
			{
				Utility.TryCreateDirectory(this._versionDir);
				Utility.TryCreateDirectory(Directory.GetParent(text).ToString());
				Utility.TryCreateDirectory(Directory.GetParent(text).ToString() + "/natives");
				downloader.AddToQueue("Minecraft " + version, text, "https://s3.amazonaws.com/Minecraft.Download/versions/1.7.10/1.7.10.jar", false);
				downloader.WaitForQueue();
			}
		}

		// Token: 0x040000A1 RID: 161
		private string _versionDir;
	}
}
