﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace ClientLauncher
{
	// Token: 0x02000020 RID: 32
	public class CheatBreakerSerializer<T>
	{
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600010E RID: 270 RVA: 0x00005681 File Offset: 0x00003881
		// (set) Token: 0x0600010F RID: 271 RVA: 0x00005689 File Offset: 0x00003889
		public bool IsLoading
		{
			get;
			set;
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000110 RID: 272 RVA: 0x00005692 File Offset: 0x00003892
		// (set) Token: 0x06000111 RID: 273 RVA: 0x0000569A File Offset: 0x0000389A
		public bool IsSaving
		{
			get;
			set;
		}

		// Token: 0x06000112 RID: 274 RVA: 0x000056A4 File Offset: 0x000038A4
		public CheatBreakerSerializer()
		{
			try
			{
				this._serializer = new DataContractSerializer(typeof(T));
			}
			catch
			{
			}
		}

		// Token: 0x06000113 RID: 275 RVA: 0x000056E4 File Offset: 0x000038E4
		public void Save(T ProObj, string Path)
		{
			if (!this.IsLoading && Path != null)
			{
				this.IsSaving = true;
				bool flag = false;
				FileStream fileStream = null;
				try
				{
					FileStream fileStream2;
					fileStream = (fileStream2 = File.Open(Path, FileMode.Create, FileAccess.Write));
					try
					{
						this._serializer.WriteObject(fileStream, ProObj);
					}
					catch
					{
						flag = true;
						throw;
					}
					finally
					{
						if (fileStream2 != null)
						{
							((IDisposable)fileStream2).Dispose();
						}
					}
				}
				catch
				{
					if (fileStream != null)
					{
						fileStream.Dispose();
						fileStream = null;
					}
					try
					{
						if (flag && Utility.FileExists(Path))
						{
							try
							{
								Utility.TryFileDelete(Path);
							}
							catch (UnauthorizedAccessException)
							{
							}
							catch (IOException)
							{
							}
						}
					}
					catch
					{
					}
				}
				this.IsSaving = false;
			}
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000057BC File Offset: 0x000039BC
		public T Load(string Path)
		{
			T result = default(T);
			if (Utility.FileExists(Path) && !this.IsSaving)
			{
				this.IsLoading = true;
				try
				{
					using (FileStream fileStream = File.OpenRead(Path))
					{
						result = (T)((object)this._serializer.ReadObject(fileStream));
					}
				}
				catch (Exception)
				{
				}
				this.IsLoading = false;
			}
			return result;
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00005838 File Offset: 0x00003A38
		public void Save(T classObj, Stream memoryStream)
		{
			if (!this.IsLoading)
			{
				this.IsSaving = true;
				if (memoryStream != null)
				{
					this._serializer.WriteObject(memoryStream, classObj);
				}
				this.IsSaving = false;
			}
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00005868 File Offset: 0x00003A68
		public void Load(out T classObj, Stream memoryStream)
		{
			classObj = default(T);
			if (!this.IsSaving)
			{
				this.IsLoading = true;
				if (memoryStream != null)
				{
					memoryStream.Seek(0L, SeekOrigin.Begin);
					classObj = (T)((object)this._serializer.ReadObject(memoryStream));
				}
				this.IsLoading = false;
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x000058B6 File Offset: 0x00003AB6
		public static void SaveObject(T classObj, string exportPath)
		{
			new CheatBreakerSerializer<T>().Save(classObj, exportPath);
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000058C4 File Offset: 0x00003AC4
		public static T LoadObject(string exportPath)
		{
			return new CheatBreakerSerializer<T>().Load(exportPath);
		}

		// Token: 0x0400007E RID: 126
		private DataContractSerializer _serializer;
	}
}
