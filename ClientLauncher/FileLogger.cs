﻿using System;
using System.IO;

namespace ClientLauncher
{
	// Token: 0x02000016 RID: 22
	public class FileLogger
	{
		// Token: 0x0600007C RID: 124 RVA: 0x00003B6C File Offset: 0x00001D6C
		public FileLogger(string filePath)
		{
			this._filePath = filePath;
			this.Open();
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00003B8C File Offset: 0x00001D8C
		public void Write(string text)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._sw != null)
				{
					try
					{
						this._sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ff: ") + text);
					}
					catch (Exception)
					{
					}
				}
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003C04 File Offset: 0x00001E04
		public void Open()
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._sw == null)
				{
					try
					{
						this._sw = File.AppendText(this._filePath);
					}
					catch (Exception)
					{
					}
				}
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003C6C File Offset: 0x00001E6C
		public void Close()
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._sw != null)
				{
					try
					{
						this._sw.Close();
					}
					catch (Exception)
					{
					}
					finally
					{
						this._sw = null;
					}
				}
			}
		}

		// Token: 0x0400003D RID: 61
		private StreamWriter _sw;

		// Token: 0x0400003E RID: 62
		private string _filePath;

		// Token: 0x0400003F RID: 63
		private object _lock = new object();
	}
}
