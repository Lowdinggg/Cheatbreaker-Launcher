﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Net;
using System.Threading;

namespace ClientLauncher
{
	// Token: 0x02000005 RID: 5
	internal class AssetDownloader
	{
		// Token: 0x0600000B RID: 11 RVA: 0x0000234C File Offset: 0x0000054C
		public void Download()
		{
			this._size = this._assets.Count;
			CheatBreakerGlobals.GetSingleton().Downloader.IsDownloading = true;
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressTextThreadSafe("Downloading Assets");
			while (!this._assets.IsEmpty)
			{
				if (this._curRunning < 10)
				{
					using (WebClient webClient = new WebClient())
					{
						webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(this.DownloadCompletedCallback);
						Asset asset;
						if (this._assets.TryDequeue(out asset))
						{
							Interlocked.Increment(ref this._curRunning);
							webClient.DownloadFileAsync(new Uri(asset.url), asset.dest);
						}
						continue;
					}
					break;
				}
			}
		}

		// Token: 0x0600000C RID: 12 RVA: 0x0000240C File Offset: 0x0000060C
		private void DownloadCompletedCallback(object sender, AsyncCompletedEventArgs e)
		{
			Interlocked.Decrement(ref this._curRunning);
			Interlocked.Increment(ref this._counter);
			double num = (double)this._counter / (double)this._size;
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressPercentageThreadSafe((int)(num * 100.0));
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressTextThreadSafe(string.Concat(new object[]
			{
				"Downloading Assets ",
				this._counter,
				"/",
				this._size
			}));
			if (this._counter == this._size)
			{
				CheatBreakerGlobals.GetSingleton().Downloader.NotifyDownloadCompleted();
			}
		}

		// Token: 0x04000004 RID: 4
		public ConcurrentQueue<Asset> _assets = new ConcurrentQueue<Asset>();

		// Token: 0x04000005 RID: 5
		private int _curRunning;

		// Token: 0x04000006 RID: 6
		private int _counter;

		// Token: 0x04000007 RID: 7
		private int _size;
	}
}
