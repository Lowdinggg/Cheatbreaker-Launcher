﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClientLauncher
{
	// Token: 0x02000011 RID: 17
	public class MinecraftServerQuery
	{
		// Token: 0x0600005F RID: 95 RVA: 0x000032E8 File Offset: 0x000014E8
		public static void PingAsync(string host, int port, int timeout, Action<MinecraftPingResult> PingCallback)
		{
			MinecraftPingResult result = new MinecraftPingResult();
			result.host = host;
			result.port = port;
			result.success = false;
			Task.Run(delegate
			{
				try
				{
					using (TcpClient tcpClient = new TcpClient())
					{
						IAsyncResult asyncResult = tcpClient.BeginConnect(host, port, null, null);
						while (!asyncResult.IsCompleted)
						{
							if (timeout <= 0)
							{
								PingCallback(result);
								tcpClient.EndConnect(asyncResult);
								return;
							}
							Thread.Sleep(10);
							timeout -= 10;
						}
						if (!tcpClient.Connected)
						{
							PingCallback(result);
							return;
						}
						using (NetworkStream stream = tcpClient.GetStream())
						{
							stream.Write(new byte[]
							{
								254
							}, 0, 1);
							byte[] array = new byte[4096];
							int num = stream.Read(array, 0, array.Length);
							if (array[0] == 255)
							{
								int num2 = num - 3;
								byte[] array2 = new byte[num2];
								Array.Copy(array, 3, array2, 0, num2);
								for (int i = 0; i < num2 / 2; i++)
								{
									byte b = array2[i * 2];
									array2[i * 2] = array2[i * 2 + 1];
									array2[i * 2 + 1] = b;
								}
								string[] array3 = Encoding.GetEncoding("ucs-2").GetString(array2, 0, num2).Split(new char[]
								{
									'§'
								});
								if (array3.Length == 3)
								{
									result.numPlayers = Convert.ToInt32(array3[1], 10);
									result.maxPlayers = Convert.ToInt32(array3[2], 10);
									result.success = true;
								}
							}
						}
					}
					PingCallback(result);
				}
				catch (Exception)
				{
					PingCallback(result);
				}
			});
		}
	}
}
