﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.Cache;
using System.Runtime.CompilerServices;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using ClientLauncher.AutoUpdater;
using ClientLauncher.Cert;
using ClientLauncher.Library;
using ClientLauncher.MojangAuth;

namespace ClientLauncher
{
	// Token: 0x0200000B RID: 11
	internal class CheatBreakerGlobals : INotifyPropertyChanged
	{
		// Token: 0x0600001E RID: 30 RVA: 0x000026C4 File Offset: 0x000008C4
		public static CheatBreakerGlobals GetSingleton()
		{
			if (CheatBreakerGlobals._instance == null)
			{
				object lockObject = CheatBreakerGlobals._lockObject;
				lock (lockObject)
				{
					if (CheatBreakerGlobals._instance == null)
					{
						CheatBreakerGlobals._instance = new CheatBreakerGlobals();
					}
				}
			}
			return CheatBreakerGlobals._instance;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001F RID: 31 RVA: 0x0000271C File Offset: 0x0000091C
		// (set) Token: 0x06000020 RID: 32 RVA: 0x00002724 File Offset: 0x00000924
		public MainWindow Window
		{
			get;
			set;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000021 RID: 33 RVA: 0x0000272D File Offset: 0x0000092D
		public Downloader Downloader
		{
			get
			{
				return this._downloader;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002735 File Offset: 0x00000935
		public MinecraftAccount SelectedAccount
		{
			get
			{
				return this.Window.ViewModel.SelectedUser;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002747 File Offset: 0x00000947
		// (set) Token: 0x06000024 RID: 36 RVA: 0x00002750 File Offset: 0x00000950
		public ObservableCollection<NewsEntry> NewsData
		{
			get
			{
				return this._newsData;
			}
			set
			{
				if (this._newsData != value)
				{
					this._newsData = value;
					this.NotifyPropertyChanged("NewsData");
					this.NotifyPropertyChanged("MainNewsContents");
					this.NotifyPropertyChanged("MainNewsAuthor");
					this.NotifyPropertyChanged("MainNewsDate");
					this.NotifyPropertyChanged("MainNewsAuthorImage");
				}
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000027A4 File Offset: 0x000009A4
		public string MainNewsContents
		{
			get
			{
				if (this._newsData == null || this._newsData.Count < 1)
				{
					return string.Empty;
				}
				return this._newsData[0].Contents;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000026 RID: 38 RVA: 0x000027D3 File Offset: 0x000009D3
		public string MainNewsTitle
		{
			get
			{
				if (this._newsData == null || this._newsData.Count < 1)
				{
					return string.Empty;
				}
				return this._newsData[0].Title;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000027 RID: 39 RVA: 0x00002802 File Offset: 0x00000A02
		public string MainNewsAuthor
		{
			get
			{
				if (this._newsData == null || this._newsData.Count < 1)
				{
					return string.Empty;
				}
				return this._newsData[0].Author;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000028 RID: 40 RVA: 0x00002831 File Offset: 0x00000A31
		public string MainNewsDate
		{
			get
			{
				if (this._newsData == null || this._newsData.Count < 1)
				{
					return string.Empty;
				}
				return this._newsData[0].Date;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000029 RID: 41 RVA: 0x00002860 File Offset: 0x00000A60
		public ImageSource MainNewsAuthorImage
		{
			get
			{
				ImageSource result;
				try
				{
					result = ((this._newsData != null && this._newsData.Count >= 1) ? new BitmapImage(new Uri(this._newsData[0].AuthorImageURL), new RequestCachePolicy(RequestCacheLevel.CacheIfAvailable)) : null);
				}
				catch
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000028C0 File Offset: 0x00000AC0
		public static void InitializeAsync()
		{
			Action arg_1F_0;
			if ((arg_1F_0 = CheatBreakerGlobals.<>c.<>9__31_0) == null)
			{
				arg_1F_0 = (CheatBreakerGlobals.<>c.<>9__31_0 = new Action(CheatBreakerGlobals.<>c.<>9.<InitializeAsync>b__31_0));
			}
			Task arg_43_0 = Task.Run(arg_1F_0);
			Action<Task> arg_43_1;
			if ((arg_43_1 = CheatBreakerGlobals.<>c.<>9__31_1) == null)
			{
				arg_43_1 = (CheatBreakerGlobals.<>c.<>9__31_1 = new Action<Task>(CheatBreakerGlobals.<>c.<>9.<InitializeAsync>b__31_1));
			}
			arg_43_0.ContinueWith(arg_43_1);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002918 File Offset: 0x00000B18
		private CheatBreakerGlobals()
		{
			try
			{
				Utility.TryCreateDirectory(CheatBreakerGlobals.GetGameDir());
			}
			catch
			{
			}
			if (!new FileCertVerify(CheatBreakerConstants.CURRENT_LAUNCHER_PATH).IsSignedByCheatBreaker())
			{
				MessageBox.Show("Launcher does not have a CheatBreaker certificate.", "", MessageBoxButton.OK, MessageBoxImage.Hand);
				throw new Exception("Launcher does not have a CheatBreaker certificate.");
			}
			this._updater = SelfUpdater.GetSingleton();
			this._downloader = Downloader.GetSingleton();
			this._versionManager = new VersionManager();
			this._assets = new AssetsManager();
			this._libraryManager = new LibraryManager();
			this.FetchNews();
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000029C4 File Offset: 0x00000BC4
		public void CheckForJRE()
		{
			if (!Utility.DirectoryExists(CheatBreakerGlobals.GetJreDir()))
			{
				this.DownloadJRE();
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000029D8 File Offset: 0x00000BD8
		public void Start()
		{
		}

		// Token: 0x0600002E RID: 46 RVA: 0x000029DC File Offset: 0x00000BDC
		public static void CaptureOutput(object sender, DataReceivedEventArgs e)
		{
			try
			{
				Log.Write("Game: " + e.Data);
			}
			catch
			{
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002A14 File Offset: 0x00000C14
		public void DownloadJRE()
		{
			Utility.TryCreateDirectory(CheatBreakerGlobals.GetJreDir());
			this._downloader.AddToQueue("JRE", CheatBreakerGlobals.GetJreDir() + "jre.zip", "https://dl.dropbox.com/s/xtq3kcrok2lltiw/jre.zip", true);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002A45 File Offset: 0x00000C45
		public static string GetRootDir()
		{
			return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CheatBreaker\\";
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002A58 File Offset: 0x00000C58
		public static string GetRealMinecraftDir()
		{
			return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft\\";
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002A6B File Offset: 0x00000C6B
		public static string GetJreDir()
		{
			return CheatBreakerGlobals.GetRootDir() + "jre\\";
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002A7C File Offset: 0x00000C7C
		public static string GetAssetsDir()
		{
			return CheatBreakerGlobals.GetRealMinecraftDir() + "assets\\";
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002A8D File Offset: 0x00000C8D
		public static string GetLibraryDir(string version)
		{
			return CheatBreakerGlobals.GetVersionDir(version) + "libraries\\";
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002AA0 File Offset: 0x00000CA0
		public static string GetVersionDir(string version)
		{
			string text = CheatBreakerGlobals.GetGameDir() + "versions\\" + version + "\\";
			try
			{
				if (!Utility.DirectoryExists(text))
				{
					Utility.TryCreateDirectory(text);
				}
			}
			catch
			{
			}
			return text;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002AE8 File Offset: 0x00000CE8
		public static string GetNativesDir(string version)
		{
			return CheatBreakerGlobals.GetVersionDir(version) + "natives\\";
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002AFA File Offset: 0x00000CFA
		public static string GetMinecraftJar(string version)
		{
			return CheatBreakerGlobals.GetVersionDir(version) + version + ".jar";
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002B0D File Offset: 0x00000D0D
		public static string GetAgentPath()
		{
			return CheatBreakerGlobals.GetRootDir() + "CBAgent.dll";
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002B1E File Offset: 0x00000D1E
		public static string GetGameDir()
		{
			return CheatBreakerGlobals.GetRootDir() + "minecraft\\";
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002B2F File Offset: 0x00000D2F
		public static bool Is64()
		{
			return Environment.Is64BitOperatingSystem;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002B36 File Offset: 0x00000D36
		public static string GetCBExePath()
		{
			return CheatBreakerGlobals.GetRootDir() + "CheatBreaker.exe";
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002B47 File Offset: 0x00000D47
		public static string GetCBDebugExePath()
		{
			return CheatBreakerGlobals.GetRootDir() + "CheatBreakerDebug.exe";
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002B58 File Offset: 0x00000D58
		public static string GetClientPatchPath()
		{
			return CheatBreakerGlobals.GetRootDir() + "client.patch";
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002B6C File Offset: 0x00000D6C
		public static string GetClientPatchWebFilename()
		{
			string result;
			try
			{
				switch (CheatBreakerSettings.GetInstance().UpdateBehavior)
				{
				case 0:
					IL_1D:
					result = "client.patch";
					return result;
				case 1:
					result = "client_experimental.patch";
					return result;
				case 2:
					result = "client_1.8.patch";
					return result;
				}
				goto IL_1D;
			}
			catch
			{
				result = "client.patch";
			}
			return result;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00002BC8 File Offset: 0x00000DC8
		public static void Debug(string s)
		{
			if (CheatBreakerConstants.DEBUG)
			{
				Console.WriteLine(s);
			}
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00002BD8 File Offset: 0x00000DD8
		public void FetchNews()
		{
			try
			{
				string arg_16_0 = "https://files.cheatbreaker.com/news_feed.xml";
				new XmlReaderSettings().DtdProcessing = DtdProcessing.Parse;
				int arg_15_0 = ServicePointManager.DefaultConnectionLimit;
				HttpWebRequest expr_20 = WebRequest.Create(arg_16_0) as HttpWebRequest;
				expr_20.Timeout = 5000;
				expr_20.ReadWriteTimeout = 5000;
				expr_20.KeepAlive = true;
				expr_20.CookieContainer = new CookieContainer();
				using (WebResponse response = expr_20.GetResponse())
				{
					RssXmlReader expr_59 = new RssXmlReader(response.GetResponseStream());
					SyndicationFeed syndicationFeed = SyndicationFeed.Load(expr_59);
					expr_59.Close();
					using (IEnumerator<SyndicationItem> enumerator = syndicationFeed.Items.GetEnumerator())
					{
						if (enumerator.MoveNext())
						{
							SyndicationItem current = enumerator.Current;
							string arg_88_0 = current.Title.Text;
							string arg_94_0 = current.Summary.Text;
							NewsEntry newsEntry = new NewsEntry();
							newsEntry.Title = ((current.Title != null) ? current.Title.Text : "");
							newsEntry.Contents = ((current.Summary != null) ? current.Summary.Text : "");
							newsEntry.Author = ((current.Authors.Count >= 1) ? current.Authors[0].Email : "");
							NewsEntry arg_136_0 = newsEntry;
							DateTimeOffset arg_113_0 = current.PublishDate;
							arg_136_0.Date = current.PublishDate.Date.ToString("d MMMM yyyy").ToUpper();
							newsEntry.AuthorImageURL = ((current.Links.Count >= 1) ? current.Links[0].Uri.ToString() : "");
							this.NewsData.Add(newsEntry);
						}
					}
				}
			}
			catch (Exception arg_1A6_0)
			{
				Log.WriteException(arg_1A6_0, "Exception retrieving news feed.");
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000041 RID: 65 RVA: 0x00002DE0 File Offset: 0x00000FE0
		// (remove) Token: 0x06000042 RID: 66 RVA: 0x00002E18 File Offset: 0x00001018
		[method: CompilerGenerated]
		[CompilerGenerated]
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x06000043 RID: 67 RVA: 0x00002E4D File Offset: 0x0000104D
		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x04000018 RID: 24
		private static CheatBreakerGlobals _instance;

		// Token: 0x04000019 RID: 25
		private static object _lockObject = new object();

		// Token: 0x0400001A RID: 26
		private Downloader _downloader;

		// Token: 0x0400001B RID: 27
		public AssetsManager _assets;

		// Token: 0x0400001C RID: 28
		public VersionManager _versionManager;

		// Token: 0x0400001D RID: 29
		public LibraryManager _libraryManager;

		// Token: 0x0400001E RID: 30
		public SelfUpdater _updater;

		// Token: 0x0400001F RID: 31
		private ObservableCollection<NewsEntry> _newsData = new ObservableCollection<NewsEntry>();

		// Token: 0x04000021 RID: 33
		public static Action PostInitialize = null;

		// Token: 0x02000056 RID: 86
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000238 RID: 568 RVA: 0x00009112 File Offset: 0x00007312
			internal void <InitializeAsync>b__31_0()
			{
				CheatBreakerGlobals.GetSingleton();
				if (CheatBreakerGlobals.PostInitialize != null)
				{
					Application.Current.Dispatcher.Invoke(CheatBreakerGlobals.PostInitialize);
				}
			}

			// Token: 0x06000239 RID: 569 RVA: 0x00009138 File Offset: 0x00007338
			internal void <InitializeAsync>b__31_1(Task t)
			{
				CheatBreakerGlobals.<>c__DisplayClass31_0 <>c__DisplayClass31_ = new CheatBreakerGlobals.<>c__DisplayClass31_0();
				<>c__DisplayClass31_.t = t;
				if (<>c__DisplayClass31_.t.IsFaulted)
				{
					Application.Current.Dispatcher.BeginInvoke(new Action(<>c__DisplayClass31_.<InitializeAsync>b__2), new object[0]);
				}
			}

			// Token: 0x0400011F RID: 287
			public static readonly CheatBreakerGlobals.<>c <>9 = new CheatBreakerGlobals.<>c();

			// Token: 0x04000120 RID: 288
			public static Action <>9__31_0;

			// Token: 0x04000121 RID: 289
			public static Action<Task> <>9__31_1;
		}
	}
}
