﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ClientLauncher
{
	// Token: 0x02000031 RID: 49
	internal class PasswordBoxBindingHack
	{
		// Token: 0x0600019D RID: 413 RVA: 0x00007138 File Offset: 0x00005338
		private static void OnBoundPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			PasswordBox passwordBox = d as PasswordBox;
			passwordBox.PasswordChanged -= new RoutedEventHandler(PasswordBoxBindingHack.HandlePasswordChanged);
			string password = (string)e.NewValue;
			if (!PasswordBoxBindingHack.GetUpdatingPassword(passwordBox))
			{
				passwordBox.Password = password;
			}
			passwordBox.PasswordChanged += new RoutedEventHandler(PasswordBoxBindingHack.HandlePasswordChanged);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000718C File Offset: 0x0000538C
		private static void OnBindPasswordChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
		{
			PasswordBox passwordBox = dp as PasswordBox;
			if (passwordBox == null)
			{
				return;
			}
			bool arg_24_0 = (bool)e.OldValue;
			bool flag = (bool)e.NewValue;
			if (arg_24_0)
			{
				passwordBox.PasswordChanged -= new RoutedEventHandler(PasswordBoxBindingHack.HandlePasswordChanged);
			}
			if (flag)
			{
				passwordBox.PasswordChanged += new RoutedEventHandler(PasswordBoxBindingHack.HandlePasswordChanged);
			}
		}

		// Token: 0x0600019F RID: 415 RVA: 0x000071E8 File Offset: 0x000053E8
		private static void HandlePasswordChanged(object sender, RoutedEventArgs e)
		{
			PasswordBox passwordBox = sender as PasswordBox;
			PasswordBoxBindingHack.SetUpdatingPassword(passwordBox, true);
			PasswordBoxBindingHack.SetBoundPassword(passwordBox, passwordBox.Password);
			PasswordBoxBindingHack.SetUpdatingPassword(passwordBox, false);
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00007216 File Offset: 0x00005416
		public static void SetBindPassword(DependencyObject dp, bool value)
		{
			dp.SetValue(PasswordBoxBindingHack.BindPasswordProperty, value);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00007229 File Offset: 0x00005429
		public static bool GetBindPassword(DependencyObject dp)
		{
			return (bool)dp.GetValue(PasswordBoxBindingHack.BindPasswordProperty);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x0000723B File Offset: 0x0000543B
		public static string GetBoundPassword(DependencyObject dp)
		{
			return (string)dp.GetValue(PasswordBoxBindingHack.BoundPasswordProperty);
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x0000724D File Offset: 0x0000544D
		public static void SetBoundPassword(DependencyObject dp, string value)
		{
			dp.SetValue(PasswordBoxBindingHack.BoundPasswordProperty, value);
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x0000725B File Offset: 0x0000545B
		private static bool GetUpdatingPassword(DependencyObject dp)
		{
			return (bool)dp.GetValue(PasswordBoxBindingHack.UpdatingPasswordProperty);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000726D File Offset: 0x0000546D
		private static void SetUpdatingPassword(DependencyObject dp, bool value)
		{
			dp.SetValue(PasswordBoxBindingHack.UpdatingPasswordProperty, value);
		}

		// Token: 0x040000BB RID: 187
		public static readonly DependencyProperty BoundPasswordProperty = DependencyProperty.RegisterAttached("BoundPassword", typeof(string), typeof(PasswordBoxBindingHack), new PropertyMetadata(string.Empty, new PropertyChangedCallback(PasswordBoxBindingHack.OnBoundPasswordChanged)));

		// Token: 0x040000BC RID: 188
		public static readonly DependencyProperty BindPasswordProperty = DependencyProperty.RegisterAttached("BindPassword", typeof(bool), typeof(PasswordBoxBindingHack), new PropertyMetadata(false, new PropertyChangedCallback(PasswordBoxBindingHack.OnBindPasswordChanged)));

		// Token: 0x040000BD RID: 189
		private static readonly DependencyProperty UpdatingPasswordProperty = DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxBindingHack), new PropertyMetadata(false));
	}
}
