﻿using System;

namespace ClientLauncher
{
	// Token: 0x0200001E RID: 30
	internal enum UpdateChannel
	{
		// Token: 0x04000077 RID: 119
		Stable,
		// Token: 0x04000078 RID: 120
		Experimental,
		// Token: 0x04000079 RID: 121
		Experimental18
	}
}
