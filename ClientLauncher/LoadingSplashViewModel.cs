﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;
using ClientLauncher.AutoUpdater;

namespace ClientLauncher
{
	// Token: 0x02000019 RID: 25
	public class LoadingSplashViewModel : INotifyPropertyChanged
	{
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600008E RID: 142 RVA: 0x00004040 File Offset: 0x00002240
		// (set) Token: 0x0600008F RID: 143 RVA: 0x00004048 File Offset: 0x00002248
		public string ProgressBarText
		{
			get
			{
				return this._progressBarText;
			}
			set
			{
				if (this._progressBarText != value)
				{
					this._progressBarText = value;
					this.NotifyPropertyChanged("ProgressBarText");
				}
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000090 RID: 144 RVA: 0x0000406A File Offset: 0x0000226A
		// (set) Token: 0x06000091 RID: 145 RVA: 0x00004072 File Offset: 0x00002272
		public int ProgressBarValue
		{
			get
			{
				return this._progressBarValue;
			}
			set
			{
				if (this._progressBarValue != value)
				{
					this._progressBarValue = value;
					this.NotifyPropertyChanged("ProgressBarValue");
				}
			}
		}

		// Token: 0x06000092 RID: 146 RVA: 0x0000408F File Offset: 0x0000228F
		public LoadingSplashViewModel(LoadingSplash view)
		{
			this._view = view;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000040A9 File Offset: 0x000022A9
		public void EndLoad()
		{
			this.ProgressBarValue = 100;
			this.ProgressBarText = "Finished...";
			WindowManager.AdvanceToMain();
		}

		// Token: 0x06000094 RID: 148 RVA: 0x000040C3 File Offset: 0x000022C3
		public void BeginLoad()
		{
			this.ProgressBarValue = 0;
			this.ProgressBarText = "Initializing...";
			CheatBreakerGlobals.PostInitialize = (Action)Delegate.Combine(CheatBreakerGlobals.PostInitialize, new Action(this.CheatBreakGlobals_PostInitialize));
			CheatBreakerGlobals.InitializeAsync();
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000040FC File Offset: 0x000022FC
		private void CheatBreakGlobals_PostInitialize()
		{
			this.ProgressBarValue = 25;
			CheatBreakerGlobals.PostInitialize = (Action)Delegate.Remove(CheatBreakerGlobals.PostInitialize, new Action(this.CheatBreakGlobals_PostInitialize));
			this.CheckForUpdate();
		}

		// Token: 0x06000096 RID: 150 RVA: 0x0000412C File Offset: 0x0000232C
		private void CheckForUpdate()
		{
			if (!CheatBreakerConstants.NO_UPDATE)
			{
				this.ProgressBarText = "Checking for updates...";
				SelfUpdater expr_17 = SelfUpdater.GetSingleton();
				expr_17.SelfUpdateComplete = (Action)Delegate.Combine(expr_17.SelfUpdateComplete, new Action(this.SelfUpdateComplete));
				expr_17.UpdateProgress = (Action<string, int>)Delegate.Combine(expr_17.UpdateProgress, new Action<string, int>(this.UpdateProgress));
				expr_17.CheckForUpdateAsync();
				return;
			}
			this.CheckForPrerequisites();
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000041A0 File Offset: 0x000023A0
		private void SelfUpdateComplete()
		{
			SelfUpdater expr_05 = SelfUpdater.GetSingleton();
			expr_05.SelfUpdateComplete = (Action)Delegate.Remove(expr_05.SelfUpdateComplete, new Action(this.SelfUpdateComplete));
			this.CheckForPrerequisites();
		}

		// Token: 0x06000098 RID: 152 RVA: 0x000041CE File Offset: 0x000023CE
		private void CheckForPrerequisites()
		{
			this.UpdateProgress("Checking for prerequisites...", 0);
			this.CheckForJRE();
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000041E4 File Offset: 0x000023E4
		private bool NeedsJRE()
		{
			bool result = true;
			try
			{
				string text = CheatBreakerGlobals.GetJreDir() + "\\bin\\server\\jvm.dll";
				if (Utility.FileExists(text) && Sha1.GetSha1Hash(text) == "0629f0f23a61cf3ee2e9ea2539a80f3b1ee82c03")
				{
					result = false;
				}
			}
			catch (Exception)
			{
			}
			return result;
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00004238 File Offset: 0x00002438
		private void CheckForJRE()
		{
			if (this.NeedsJRE())
			{
				this.UpdateProgress("Downloading JRE...", 0);
				AsyncDownloader expr_19 = new AsyncDownloader();
				expr_19.DownloadComplete = (Action<AsyncDownloader, bool>)Delegate.Combine(expr_19.DownloadComplete, new Action<AsyncDownloader, bool>(this.JREDownloadComplete));
				expr_19.UpdateProgress = (Action<AsyncDownloader, string, int>)Delegate.Combine(expr_19.UpdateProgress, new Action<AsyncDownloader, string, int>(this.Downloader_UpdateProgress));
				expr_19.DownloadFileAsync("http://jre.cheatbreaker.com/jre.zip", CheatBreakerGlobals.GetRootDir() + "jre.zip");
				return;
			}
			this.EndLoad();
		}

		// Token: 0x0600009B RID: 155 RVA: 0x000042C4 File Offset: 0x000024C4
		private void JREDownloadComplete(AsyncDownloader downloader, bool success)
		{
			downloader.DownloadComplete = (Action<AsyncDownloader, bool>)Delegate.Remove(downloader.DownloadComplete, new Action<AsyncDownloader, bool>(this.JREDownloadComplete));
			downloader.UpdateProgress = (Action<AsyncDownloader, string, int>)Delegate.Remove(downloader.UpdateProgress, new Action<AsyncDownloader, string, int>(this.Downloader_UpdateProgress));
			if (!success)
			{
				Dispatcher arg_76_0 = Application.Current.Dispatcher;
				Action arg_76_1;
				if ((arg_76_1 = LoadingSplashViewModel.<>c.<>9__18_0) == null)
				{
					arg_76_1 = (LoadingSplashViewModel.<>c.<>9__18_0 = new Action(LoadingSplashViewModel.<>c.<>9.<JREDownloadComplete>b__18_0));
				}
				arg_76_0.BeginInvoke(arg_76_1, new object[0]);
				return;
			}
			try
			{
				this.UpdateProgress("Installing JRE...", 0);
				string jreDir = CheatBreakerGlobals.GetJreDir();
				Utility.TryDirectoryDelete(jreDir, true);
				Utility.TryCreateDirectory(jreDir);
				using (FileStream fileStream = new FileStream(downloader._targetFile, FileMode.Open))
				{
					using (ZipArchive zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read))
					{
						zipArchive.ExtractToDirectory(jreDir, true);
					}
				}
				Utility.TryFileDelete(downloader._targetFile);
			}
			catch (Exception arg_E7_0)
			{
				Exception ex2 = arg_E7_0;
				Exception ex = ex2;
				Log.WriteException(ex, "Failed to extract JRE from: " + downloader._targetFile);
				Application.Current.Dispatcher.BeginInvoke(new Action(delegate
				{
					throw new Exception("Failed while trying to install JRE: " + ex.ToString());
				}), new object[0]);
				return;
			}
			this.EndLoad();
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00004430 File Offset: 0x00002630
		private void Downloader_UpdateProgress(AsyncDownloader downloader, string statusText, int progress)
		{
			this.UpdateProgress(statusText, progress);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x0000443A File Offset: 0x0000263A
		private void CheckForPrerequisitesComplete()
		{
			this.EndLoad();
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00004442 File Offset: 0x00002642
		private void UpdateProgress(string statusText, int progress)
		{
			if (statusText != null)
			{
				this.ProgressBarText = statusText;
			}
			if (progress >= 0)
			{
				this.ProgressBarValue = progress;
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x0600009F RID: 159 RVA: 0x0000445C File Offset: 0x0000265C
		// (remove) Token: 0x060000A0 RID: 160 RVA: 0x00004494 File Offset: 0x00002694
		[method: CompilerGenerated]
		[CompilerGenerated]
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x060000A1 RID: 161 RVA: 0x000044C9 File Offset: 0x000026C9
		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x04000045 RID: 69
		private LoadingSplash _view;

		// Token: 0x04000046 RID: 70
		private string _progressBarText = string.Empty;

		// Token: 0x04000047 RID: 71
		private int _progressBarValue;

		// Token: 0x0200005C RID: 92
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000247 RID: 583 RVA: 0x000096B5 File Offset: 0x000078B5
			internal void <JREDownloadComplete>b__18_0()
			{
				throw new Exception("Failed while trying to download JRE to install. Check log files for additional information.");
			}

			// Token: 0x0400012F RID: 303
			public static readonly LoadingSplashViewModel.<>c <>9 = new LoadingSplashViewModel.<>c();

			// Token: 0x04000130 RID: 304
			public static Action <>9__18_0;
		}
	}
}
