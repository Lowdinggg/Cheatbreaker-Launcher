﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000013 RID: 19
	public interface ILauncherCallback
	{
		// Token: 0x06000066 RID: 102
		void UpdateLauncherText(string statusText);

		// Token: 0x06000067 RID: 103
		void UpdateLauncherProgress(int progress);

		// Token: 0x06000068 RID: 104
		void UpdateLaunching(bool launching);

		// Token: 0x06000069 RID: 105
		void UpdateGameRunning(bool running);
	}
}
