﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;

namespace ClientLauncher
{
	// Token: 0x02000015 RID: 21
	public class LoadingSplash : Window, IComponentConnector
	{
		// Token: 0x06000077 RID: 119 RVA: 0x00003ACC File Offset: 0x00001CCC
		public LoadingSplash()
		{
			this._viewModel = new LoadingSplashViewModel(this);
			base.DataContext = this._viewModel;
			this.InitializeComponent();
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003AF2 File Offset: 0x00001CF2
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			this._viewModel.BeginLoad();
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00003388 File Offset: 0x00001588
		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				base.DragMove();
			}
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00003B00 File Offset: 0x00001D00
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/ClientLauncher;component/windows/loadingsplash/loadingsplash.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00003B30 File Offset: 0x00001D30
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((LoadingSplash)target).Loaded += new RoutedEventHandler(this.Window_Loaded);
				((LoadingSplash)target).MouseDown += new MouseButtonEventHandler(this.Window_MouseDown);
				return;
			}
			this._contentLoaded = true;
		}

		// Token: 0x0400003B RID: 59
		private LoadingSplashViewModel _viewModel;

		// Token: 0x0400003C RID: 60
		private bool _contentLoaded;
	}
}
