﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ClientLauncher
{
	// Token: 0x02000009 RID: 9
	public class AsyncDownloader
	{
		// Token: 0x06000018 RID: 24 RVA: 0x00002523 File Offset: 0x00000723
		private void NotifyProgress(string text, int progress)
		{
			if (this.UpdateProgress != null)
			{
				Application.Current.Dispatcher.BeginInvoke(this.UpdateProgress, new object[]
				{
					this,
					text,
					progress
				});
			}
		}

		// Token: 0x06000019 RID: 25 RVA: 0x0000255C File Offset: 0x0000075C
		public void DownloadFileAsync(string urlSource, string targetFile)
		{
			this._urlSource = urlSource;
			this._targetFile = targetFile;
			Task arg_63_0 = Task.Run(delegate
			{
				try
				{
					object obj = new object();
					object obj2 = obj;
					lock (obj2)
					{
						using (WebClient webClient = new WebClient())
						{
							webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.Client_DownloadProgressChanged);
							webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(this.Client_DownloadFileCompleted);
							webClient.DownloadFileAsync(new Uri(urlSource), targetFile, obj);
							Monitor.Wait(obj);
							if (this.DownloadComplete != null)
							{
								Application.Current.Dispatcher.BeginInvoke(this.DownloadComplete, new object[]
								{
									this,
									!this._erroredOrCancelled
								});
							}
						}
					}
				}
				catch (Exception expr_CC)
				{
					Log.WriteException(expr_CC, "Failed to download file from: " + urlSource + " for " + targetFile);
					throw expr_CC;
				}
			});
			Action<Task> arg_63_1;
			if ((arg_63_1 = AsyncDownloader.<>c.<>9__6_1) == null)
			{
				arg_63_1 = (AsyncDownloader.<>c.<>9__6_1 = new Action<Task>(AsyncDownloader.<>c.<>9.<DownloadFileAsync>b__6_1));
			}
			arg_63_0.ContinueWith(arg_63_1);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000025D4 File Offset: 0x000007D4
		private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			this._erroredOrCancelled = false;
			if (e.Cancelled)
			{
				Log.Write("Download was cancelled?");
				this._erroredOrCancelled = true;
			}
			if (e.Error != null)
			{
				Log.WriteException(e.Error, "Exception when downloading file");
				this._erroredOrCancelled = true;
			}
			this.NotifyProgress(null, 100);
			object userState = e.UserState;
			lock (userState)
			{
				Monitor.Pulse(e.UserState);
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002664 File Offset: 0x00000864
		private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			this.NotifyProgress(null, Math.Min(100, Math.Max(0, e.ProgressPercentage)));
		}

		// Token: 0x0400000D RID: 13
		public Action<AsyncDownloader, bool> DownloadComplete;

		// Token: 0x0400000E RID: 14
		public Action<AsyncDownloader, string, int> UpdateProgress;

		// Token: 0x0400000F RID: 15
		public bool _erroredOrCancelled;

		// Token: 0x04000010 RID: 16
		public string _urlSource = string.Empty;

		// Token: 0x04000011 RID: 17
		public string _targetFile = string.Empty;

		// Token: 0x02000054 RID: 84
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000233 RID: 563 RVA: 0x000090B0 File Offset: 0x000072B0
			internal void <DownloadFileAsync>b__6_1(Task t)
			{
				AsyncDownloader.<>c__DisplayClass6_1 <>c__DisplayClass6_ = new AsyncDownloader.<>c__DisplayClass6_1();
				<>c__DisplayClass6_.t = t;
				if (<>c__DisplayClass6_.t.IsFaulted)
				{
					Application.Current.Dispatcher.BeginInvoke(new Action(<>c__DisplayClass6_.<DownloadFileAsync>b__2), new object[0]);
				}
			}

			// Token: 0x0400011C RID: 284
			public static readonly AsyncDownloader.<>c <>9 = new AsyncDownloader.<>c();

			// Token: 0x0400011D RID: 285
			public static Action<Task> <>9__6_1;
		}
	}
}
