﻿using System;

namespace ClientLauncher.AutoUpdater
{
	// Token: 0x02000050 RID: 80
	internal class AutoUpdateFile
	{
		// Token: 0x06000225 RID: 549 RVA: 0x00008F18 File Offset: 0x00007118
		public AutoUpdateFile(string endpoint, string filePath, bool checkSignature)
		{
			this.endpoint = endpoint;
			this.filePath = filePath;
			this.checkSignature = checkSignature;
		}

		// Token: 0x06000226 RID: 550 RVA: 0x00008F35 File Offset: 0x00007135
		public string getEndpoint()
		{
			return this.endpoint;
		}

		// Token: 0x06000227 RID: 551 RVA: 0x00008F3D File Offset: 0x0000713D
		public string getFilePath()
		{
			return this.filePath;
		}

		// Token: 0x06000228 RID: 552 RVA: 0x00008F45 File Offset: 0x00007145
		public bool shouldCheckSignature()
		{
			return this.checkSignature;
		}

		// Token: 0x04000112 RID: 274
		private string endpoint;

		// Token: 0x04000113 RID: 275
		private string filePath;

		// Token: 0x04000114 RID: 276
		private bool checkSignature;
	}
}
