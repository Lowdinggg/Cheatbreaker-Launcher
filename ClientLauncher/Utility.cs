﻿using System;
using System.IO;

namespace ClientLauncher
{
	// Token: 0x02000022 RID: 34
	public static class Utility
	{
		// Token: 0x0600012C RID: 300 RVA: 0x00005AD8 File Offset: 0x00003CD8
		public static void TryCreateDirectory(string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return;
			}
			try
			{
				Directory.CreateDirectory(path);
			}
			catch (Exception arg_1E_0)
			{
				Log.WriteException(arg_1E_0, "Failed to create directory: " + path);
			}
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00005B1C File Offset: 0x00003D1C
		public static string DirectoryGetParent(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return null;
			}
			try
			{
				return Directory.GetParent(path).ToString();
			}
			catch (Exception arg_24_0)
			{
				Log.WriteException(arg_24_0, "Tried getting directory parent: " + path);
			}
			return null;
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00005B68 File Offset: 0x00003D68
		public static bool FileExists(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return false;
			}
			try
			{
				if (File.Exists(path))
				{
					return true;
				}
			}
			catch (Exception arg_24_0)
			{
				Log.WriteException(arg_24_0, "Tried checking if file exists: " + path);
			}
			return false;
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00005BB4 File Offset: 0x00003DB4
		public static bool DirectoryExists(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return false;
			}
			try
			{
				if (Directory.Exists(path))
				{
					return true;
				}
			}
			catch (Exception arg_24_0)
			{
				Log.WriteException(arg_24_0, "Tried checking if directory exists: " + path);
			}
			return false;
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00005C00 File Offset: 0x00003E00
		public static bool TryFileDelete(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return false;
			}
			try
			{
				File.Delete(path);
				return true;
			}
			catch (Exception arg_20_0)
			{
				Log.WriteException(arg_20_0, "Failed to delete file: " + path);
			}
			return false;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00005C48 File Offset: 0x00003E48
		public static bool TryDirectoryDelete(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return false;
			}
			try
			{
				Directory.Delete(path);
				return true;
			}
			catch (Exception arg_20_0)
			{
				Log.WriteException(arg_20_0, "Failed to delete directory: " + path);
			}
			return false;
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00005C90 File Offset: 0x00003E90
		public static bool TryDirectoryDelete(string path, bool recursive)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				return false;
			}
			try
			{
				Directory.Delete(path, recursive);
				return true;
			}
			catch (Exception arg_21_0)
			{
				Log.WriteException(arg_21_0, "Failed to delete directory: " + path);
			}
			return false;
		}
	}
}
