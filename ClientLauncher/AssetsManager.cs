﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Newtonsoft.Json.Linq;

namespace ClientLauncher
{
	// Token: 0x02000003 RID: 3
	internal class AssetsManager
	{
		// Token: 0x06000007 RID: 7 RVA: 0x000020C0 File Offset: 0x000002C0
		public AssetsManager()
		{
			this._assetsDir = CheatBreakerGlobals.GetAssetsDir();
			this._indexesDir = CheatBreakerGlobals.GetAssetsDir() + "indexes\\";
			this._objectsDir = CheatBreakerGlobals.GetAssetsDir() + "objects\\";
			Utility.TryCreateDirectory(this._assetsDir);
			Utility.TryCreateDirectory(this._indexesDir);
			Utility.TryCreateDirectory(this._objectsDir);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x0000212C File Offset: 0x0000032C
		public void GetForVersion(Downloader dl, string version, JObject assetsJsonObject)
		{
			IEnumerable<JProperty> arg_53_0 = ((JObject)assetsJsonObject["objects"]).Properties();
			Func<JProperty, string> arg_53_1;
			if ((arg_53_1 = AssetsManager.<>c.<>9__4_0) == null)
			{
				arg_53_1 = (AssetsManager.<>c.<>9__4_0 = new Func<JProperty, string>(AssetsManager.<>c.<>9.<GetForVersion>b__4_0));
			}
			Func<JProperty, IndexObject> arg_53_2;
			if ((arg_53_2 = AssetsManager.<>c.<>9__4_1) == null)
			{
				arg_53_2 = (AssetsManager.<>c.<>9__4_1 = new Func<JProperty, IndexObject>(AssetsManager.<>c.<>9.<GetForVersion>b__4_1));
			}
			Dictionary<string, IndexObject> arg_92_0 = arg_53_0.ToDictionary(arg_53_1, arg_53_2);
			AssetDownloader assetDownloader = new AssetDownloader();
			CheatBreakerGlobals.GetSingleton().Downloader.IsDownloading = true;
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressTextThreadSafe("Checking Assets");
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressPercentageThreadSafe(0);
			foreach (KeyValuePair<string, IndexObject> current in arg_92_0)
			{
				string hash = current.Value.hash;
				string str = hash.Substring(0, 2);
				string text = this._objectsDir + str + "\\" + hash;
				Utility.TryCreateDirectory(Directory.GetParent(text).ToString());
				if (Utility.FileExists(text))
				{
					if (!Sha1.GetSha1Hash(text).Equals(hash))
					{
						assetDownloader._assets.Enqueue(new Asset(text, "http://resources.download.minecraft.net/" + str + "/" + hash));
					}
				}
				else
				{
					assetDownloader._assets.Enqueue(new Asset(text, "http://resources.download.minecraft.net/" + str + "/" + hash));
				}
			}
			CheatBreakerGlobals.GetSingleton().Downloader.NotifyDownloadCompleted();
			if (!assetDownloader._assets.IsEmpty)
			{
				Log.Write(string.Format("Downloading {0} assets.", assetDownloader._assets.Count));
				assetDownloader.Download();
			}
		}

		// Token: 0x04000001 RID: 1
		private string _indexesDir;

		// Token: 0x04000002 RID: 2
		private string _objectsDir;

		// Token: 0x04000003 RID: 3
		private string _assetsDir;

		// Token: 0x02000051 RID: 81
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600022B RID: 555 RVA: 0x00008F59 File Offset: 0x00007159
			internal string <GetForVersion>b__4_0(JProperty p)
			{
				return p.Name;
			}

			// Token: 0x0600022C RID: 556 RVA: 0x00008F61 File Offset: 0x00007161
			internal IndexObject <GetForVersion>b__4_1(JProperty p)
			{
				return p.Value.ToObject<IndexObject>();
			}

			// Token: 0x04000115 RID: 277
			public static readonly AssetsManager.<>c <>9 = new AssetsManager.<>c();

			// Token: 0x04000116 RID: 278
			public static Func<JProperty, string> <>9__4_0;

			// Token: 0x04000117 RID: 279
			public static Func<JProperty, IndexObject> <>9__4_1;
		}
	}
}
