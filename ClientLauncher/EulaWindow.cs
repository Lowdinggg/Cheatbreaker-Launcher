﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace ClientLauncher
{
	// Token: 0x02000012 RID: 18
	public class EulaWindow : Window, IComponentConnector
	{
		// Token: 0x06000061 RID: 97 RVA: 0x00003362 File Offset: 0x00001562
		public EulaWindow()
		{
			this._viewModel = new EulaWindowViewModel(this);
			base.DataContext = this._viewModel;
			this.InitializeComponent();
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00003388 File Offset: 0x00001588
		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				base.DragMove();
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00003398 File Offset: 0x00001598
		private void Window_Closed(object sender, EventArgs e)
		{
			WindowManager.NotifyEulaClosed();
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000033A0 File Offset: 0x000015A0
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/ClientLauncher;component/windows/eula/eulawindow.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x000033D0 File Offset: 0x000015D0
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((EulaWindow)target).MouseDown += new MouseButtonEventHandler(this.Window_MouseDown);
				((EulaWindow)target).Closed += new EventHandler(this.Window_Closed);
				return;
			}
			if (connectionId != 2)
			{
				this._contentLoaded = true;
				return;
			}
			this.EulaTextBox = (TextBox)target;
		}

		// Token: 0x04000032 RID: 50
		private EulaWindowViewModel _viewModel;

		// Token: 0x04000033 RID: 51
		internal TextBox EulaTextBox;

		// Token: 0x04000034 RID: 52
		private bool _contentLoaded;
	}
}
