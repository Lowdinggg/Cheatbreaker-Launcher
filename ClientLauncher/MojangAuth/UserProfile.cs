﻿using System;
using System.Collections.Generic;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x02000040 RID: 64
	internal class UserProfile
	{
		// Token: 0x040000F0 RID: 240
		public string id;

		// Token: 0x040000F1 RID: 241
		public List<ResponseProperty> properties;
	}
}
