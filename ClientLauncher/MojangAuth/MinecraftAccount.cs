﻿using System;
using System.Net.Cache;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x02000039 RID: 57
	public class MinecraftAccount
	{
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00007952 File Offset: 0x00005B52
		public string DetailedDisplayName
		{
			get
			{
				return string.Format("{0} ({1})", this.DisplayName, this.UUIDTrimmed);
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001CC RID: 460 RVA: 0x0000796A File Offset: 0x00005B6A
		public string DisplayName
		{
			get
			{
				return this._displayName ?? string.Empty;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001CD RID: 461 RVA: 0x0000797B File Offset: 0x00005B7B
		public string UUID
		{
			get
			{
				return this._uuid ?? string.Empty;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001CE RID: 462 RVA: 0x0000798C File Offset: 0x00005B8C
		public string UUIDTrimmed
		{
			get
			{
				return this._uuid.Replace("-", "");
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001CF RID: 463 RVA: 0x000079A3 File Offset: 0x00005BA3
		public string UserID
		{
			get
			{
				return this._userid ?? string.Empty;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001D0 RID: 464 RVA: 0x000079B4 File Offset: 0x00005BB4
		public string AccessToken
		{
			get
			{
				return this._accessToken ?? string.Empty;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x000079C5 File Offset: 0x00005BC5
		public string Username
		{
			get
			{
				return this._username ?? string.Empty;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001D2 RID: 466 RVA: 0x000079D6 File Offset: 0x00005BD6
		public ImageSource DisplayIcon
		{
			get
			{
				if (this._displayIcon == null)
				{
					this._displayIcon = new BitmapImage(new Uri("https://visage.surgeplay.com/face/" + this.UUIDTrimmed), new RequestCachePolicy(RequestCacheLevel.CacheIfAvailable));
				}
				return this._displayIcon;
			}
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00007A0C File Offset: 0x00005C0C
		public MinecraftAccount(string displayName, string accessToken, string userid, string uuid, string username)
		{
			this._displayName = displayName;
			this._accessToken = accessToken;
			this._userid = userid;
			this._uuid = uuid;
			this._username = username;
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x00007A39 File Offset: 0x00005C39
		public override string ToString()
		{
			return this.DisplayName;
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00007A41 File Offset: 0x00005C41
		public void ChangeAccessToken(string accessToken)
		{
			this._accessToken = accessToken;
		}

		// Token: 0x040000D2 RID: 210
		private string _displayName;

		// Token: 0x040000D3 RID: 211
		private string _uuid;

		// Token: 0x040000D4 RID: 212
		private string _userid;

		// Token: 0x040000D5 RID: 213
		private string _accessToken;

		// Token: 0x040000D6 RID: 214
		private string _username;

		// Token: 0x040000D7 RID: 215
		private BitmapImage _displayIcon;
	}
}
