﻿using System;

namespace ClientLauncher.MojangAuth.Payloads
{
	// Token: 0x02000046 RID: 70
	internal class ValidatePayload : Payload
	{
		// Token: 0x060001ED RID: 493 RVA: 0x00008279 File Offset: 0x00006479
		public ValidatePayload(string accessToken, string clientToken)
		{
			this.accessToken = accessToken;
			this.clientToken = clientToken;
		}

		// Token: 0x040000F6 RID: 246
		public string accessToken;

		// Token: 0x040000F7 RID: 247
		public string clientToken;
	}
}
