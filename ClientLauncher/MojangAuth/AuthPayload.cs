﻿using System;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x0200003B RID: 59
	internal class AuthPayload : Payload
	{
		// Token: 0x060001E1 RID: 481 RVA: 0x000081F2 File Offset: 0x000063F2
		public AuthPayload(string username, string password, string clientToken, LoginAgent agent)
		{
			this.username = username;
			this.password = password;
			this.clientToken = clientToken;
			this.agent = agent;
			this.requestUser = true;
		}

		// Token: 0x040000E2 RID: 226
		public LoginAgent agent;

		// Token: 0x040000E3 RID: 227
		public string username;

		// Token: 0x040000E4 RID: 228
		public string password;

		// Token: 0x040000E5 RID: 229
		public string clientToken;

		// Token: 0x040000E6 RID: 230
		public bool requestUser;
	}
}
