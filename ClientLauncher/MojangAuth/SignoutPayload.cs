﻿using System;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x0200003C RID: 60
	internal class SignoutPayload : Payload
	{
		// Token: 0x060001E2 RID: 482 RVA: 0x0000821E File Offset: 0x0000641E
		public SignoutPayload(string clientToken, string accessToken)
		{
			this.clientToken = clientToken;
			this.accessToken = accessToken;
		}

		// Token: 0x040000E7 RID: 231
		public string clientToken;

		// Token: 0x040000E8 RID: 232
		public string accessToken;
	}
}
