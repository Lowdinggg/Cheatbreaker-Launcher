﻿using System;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x02000038 RID: 56
	internal class LoginProfile
	{
		// Token: 0x040000CF RID: 207
		public string id;

		// Token: 0x040000D0 RID: 208
		public string name;

		// Token: 0x040000D1 RID: 209
		public bool legacy;
	}
}
