﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClientLauncher.MojangAuth.Payloads;
using ClientLauncher.MojangAuth.Responses;
using Newtonsoft.Json.Linq;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x0200003A RID: 58
	public class MojangProfiles
	{
		// Token: 0x060001D6 RID: 470 RVA: 0x00007A4C File Offset: 0x00005C4C
		public MojangProfiles()
		{
			try
			{
				JObject jObject = JObject.Parse(File.ReadAllText(CheatBreakerGlobals.GetRealMinecraftDir() + "launcher_profiles.json"));
				try
				{
					this.profiles = jObject.GetValue("profiles");
					this.selectedProfile = jObject.GetValue("selectedProfile");
					this.launcherVersion = jObject.GetValue("launcherVersion");
					this.settings = jObject.GetValue("settings");
					this.analyticsFailcount = jObject.GetValue("analyticsFailcount");
					this.analyticsToken = jObject.GetValue("analyticsToken");
					this.clientToken = (JValue)jObject.GetValue("clientToken");
					this.authenticationDatabase = (JObject)jObject.GetValue("authenticationDatabase");
					this.selectedUser = (JObject)jObject.GetValue("selectedUser");
					if (this.authenticationDatabase == null)
					{
						this.authenticationDatabase = new JObject();
					}
				}
				catch (Exception arg_E2_0)
				{
					Log.WriteException(arg_E2_0, "Failed to load profiles.");
				}
				this.mojangProfilesObject = jObject;
			}
			catch
			{
			}
			if (this.selectedUser == null)
			{
				this.selectedUser = new JObject();
			}
			if (this.authenticationDatabase == null)
			{
				this.authenticationDatabase = new JObject();
			}
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x00007B90 File Offset: 0x00005D90
		public string GetClientToken()
		{
			if (this.clientToken == null || this.clientToken.Value == null)
			{
				return string.Empty;
			}
			return this.clientToken.Value.ToString();
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x00007BBD File Offset: 0x00005DBD
		public string GetSelectedUserUUID()
		{
			if (this.selectedUser == null || this.selectedUser["profile"] == null)
			{
				return string.Empty;
			}
			return this.selectedUser["profile"].ToString().Trim();
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00007BF9 File Offset: 0x00005DF9
		public void SetClientToken(string token)
		{
			this.clientToken.Value = token;
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00007C08 File Offset: 0x00005E08
		public void SetSelectedUser(MinecraftAccount user)
		{
			if (this.selectedUser == null)
			{
				this.selectedUser = new JObject();
			}
			if (user != null)
			{
				this.selectedUser["account"] = user.UserID;
				this.selectedUser["profile"] = user.UUID;
				return;
			}
			this.selectedUser["account"] = null;
			this.selectedUser["profile"] = null;
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00007C84 File Offset: 0x00005E84
		public void SetAccounts(List<MinecraftAccount> accounts)
		{
			this.authenticationDatabase.RemoveAll();
			foreach (MinecraftAccount current in accounts)
			{
				try
				{
					JObject jObject = new JObject();
					jObject.Add("accessToken", current.AccessToken);
					jObject.Add("username", current.Username);
					JObject jObject2 = new JObject();
					JObject jObject3 = new JObject();
					jObject3.Add("displayName", current.DisplayName);
					jObject2.Add(current.UUID, jObject3);
					jObject.Add("profiles", jObject2);
					this.authenticationDatabase.Add(current.UserID, jObject);
				}
				catch (Exception arg_A8_0)
				{
					Log.WriteException(arg_A8_0, "Exception adding account");
				}
			}
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00007D78 File Offset: 0x00005F78
		public List<MinecraftAccount> GetAccounts()
		{
			List<MinecraftAccount> list = new List<MinecraftAccount>();
			try
			{
				foreach (KeyValuePair<string, JToken> current in this.authenticationDatabase)
				{
					JObject expr_30 = (JObject)current.Value;
					string accessToken = expr_30.GetValue("accessToken").ToString();
					string username = expr_30.GetValue("username").ToString();
					string uuid = "";
					string displayName = "";
					string userid = current.Key.ToString();
					using (IEnumerator<JToken> enumerator2 = ((IEnumerable<JToken>)expr_30["profiles"]).GetEnumerator())
					{
						if (enumerator2.MoveNext())
						{
							JProperty expr_8F = (JProperty)enumerator2.Current;
							uuid = expr_8F.Name;
							displayName = ((JObject)expr_8F.Value).GetValue("displayName").ToString();
						}
					}
					MinecraftAccount account = new MinecraftAccount(displayName, accessToken, userid, uuid, username);
					if (this.GetSelectedUserUUID().Equals(account.UUID) && !this.ValidateAccount(account))
					{
						Log.Write(string.Format("Excluding account {0} with access token {1} because it failed validation.", account.DetailedDisplayName, account.AccessToken));
					}
					else
					{
						Log.Write(string.Format("Adding account {0} with access token {1}.", account.DetailedDisplayName, account.AccessToken));
						if (!list.Any((MinecraftAccount x) => x.UUID.Equals(account.UUID)))
						{
							list.Add(account);
						}
					}
				}
			}
			catch
			{
			}
			return list;
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00007F60 File Offset: 0x00006160
		public bool ValidateAccount(MinecraftAccount acc)
		{
			Log.Write(string.Format("Validating account {0} with access token {1}", acc.DetailedDisplayName, acc.AccessToken));
			Payload arg_35_0 = new ValidatePayload(acc.AccessToken, this.GetClientToken());
			int num = 0;
			ValidateResponse validateResponse = (ValidateResponse)Authenticator.GetResponse<ValidateResponse>(arg_35_0, "validate", out num);
			if (validateResponse != null && validateResponse.errorMessage != null)
			{
				if (num != 204)
				{
					Log.Write(string.Format("Status code {0}. Refreshing...", num));
					MinecraftAccount minecraftAccount = this.Refresh(acc);
					if (minecraftAccount == null)
					{
						Log.Write("Failed to refresh. Not valid.");
						return false;
					}
					if (string.IsNullOrEmpty(minecraftAccount.AccessToken))
					{
						Log.Write("Failed to refresh due to empty access token in response. Not valid.");
						return false;
					}
					Log.Write(string.Format("Refresh successful. Changing access token to {0} from {1} for {2}.", minecraftAccount.AccessToken, acc.AccessToken, acc.DetailedDisplayName));
					acc.ChangeAccessToken(minecraftAccount.AccessToken);
				}
				else
				{
					Log.Write("Status code 204. Validated.");
				}
			}
			else
			{
				Log.Write("No error. Validated.");
			}
			return true;
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00008054 File Offset: 0x00006254
		private MinecraftAccount Refresh(MinecraftAccount account)
		{
			Payload arg_1A_0 = new RefreshPayload(account.AccessToken, this.GetClientToken());
			int num = 0;
			RefreshResponse refreshResponse = (RefreshResponse)Authenticator.GetResponse<RefreshResponse>(arg_1A_0, "refresh", out num);
			if (refreshResponse != null && num != 400 && num != 403 && refreshResponse.errorMessage == null)
			{
				MinecraftAccount result = new MinecraftAccount(account.DisplayName, refreshResponse.accessToken, account.UUID, account.UserID, account.Username);
				Log.Write(string.Concat(new string[]
				{
					"Refreshed received access token \"",
					refreshResponse.accessToken ?? string.Empty,
					"\" and original token was \"",
					account.AccessToken ?? string.Empty,
					"\""
				}));
				return result;
			}
			return null;
		}

		// Token: 0x060001DF RID: 479 RVA: 0x0000811C File Offset: 0x0000631C
		private string GetStringValue()
		{
			return new JObject
			{
				{
					"settings",
					this.settings
				},
				{
					"launcherVersion",
					this.launcherVersion
				},
				{
					"clientToken",
					this.clientToken
				},
				{
					"profiles",
					this.profiles
				},
				{
					"analyticsFailcount",
					this.analyticsFailcount
				},
				{
					"analyticsToken",
					this.analyticsToken
				},
				{
					"selectedProfile",
					this.selectedProfile
				},
				{
					"authenticationDatabase",
					this.authenticationDatabase
				},
				{
					"selectedUser",
					this.selectedUser
				}
			}.ToString();
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x000081CC File Offset: 0x000063CC
		public void Save()
		{
			Log.Write("Saving profiles.");
			File.WriteAllText(Path.Combine(CheatBreakerGlobals.GetRealMinecraftDir(), "launcher_profiles.json"), this.GetStringValue());
		}

		// Token: 0x040000D8 RID: 216
		private JObject mojangProfilesObject;

		// Token: 0x040000D9 RID: 217
		private JToken settings;

		// Token: 0x040000DA RID: 218
		private JToken launcherVersion;

		// Token: 0x040000DB RID: 219
		private JToken analyticsFailcount;

		// Token: 0x040000DC RID: 220
		private JToken analyticsToken;

		// Token: 0x040000DD RID: 221
		private JToken profiles;

		// Token: 0x040000DE RID: 222
		private JToken selectedProfile;

		// Token: 0x040000DF RID: 223
		private JValue clientToken;

		// Token: 0x040000E0 RID: 224
		private JObject authenticationDatabase;

		// Token: 0x040000E1 RID: 225
		private JObject selectedUser;
	}
}
