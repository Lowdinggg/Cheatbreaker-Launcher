﻿using System;
using System.IO;
using System.Net;
using System.Text;
using ClientLauncher.MojangAuth.Responses;
using Newtonsoft.Json;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x02000036 RID: 54
	internal static class Authenticator
	{
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x0000771B File Offset: 0x0000591B
		// (set) Token: 0x060001C7 RID: 455 RVA: 0x00007722 File Offset: 0x00005922
		public static string ClientToken
		{
			get
			{
				return Authenticator._clientToken;
			}
			set
			{
				Authenticator._clientToken = value;
			}
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x0000772C File Offset: 0x0000592C
		public static Response GetResponse<ResponseType>(Payload payload, string endpoint, out int errorCode) where ResponseType : Response, new()
		{
			errorCode = 0;
			Response result;
			try
			{
				WebRequest webRequest = WebRequest.Create("https://authserver.mojang.com/" + endpoint);
				webRequest.Method = "POST";
				string s = JsonConvert.SerializeObject(payload);
				byte[] bytes = Encoding.UTF8.GetBytes(s);
				webRequest.ContentType = "application/json";
				webRequest.ContentLength = (long)bytes.Length;
				Stream expr_51 = webRequest.GetRequestStream();
				expr_51.Write(bytes, 0, bytes.Length);
				expr_51.Close();
				WebResponse response;
				StreamReader streamReader;
				string text;
				try
				{
					response = webRequest.GetResponse();
					if (response is HttpWebResponse)
					{
						errorCode = (int)(response as HttpWebResponse).StatusCode;
					}
				}
				catch (WebException ex)
				{
					Log.WriteException(ex, "GetResponse failed.");
					if (ex.Response is HttpWebResponse)
					{
						errorCode = (int)(ex.Response as HttpWebResponse).StatusCode;
					}
					ResponseType responseType = Activator.CreateInstance<ResponseType>();
					responseType.errorMessage = string.Empty;
					try
					{
						StreamReader streamReader2;
						streamReader = (streamReader2 = new StreamReader(ex.Response.GetResponseStream()));
						try
						{
							text = streamReader.ReadToEnd();
							responseType.errorMessage = text;
						}
						finally
						{
							if (streamReader2 != null)
							{
								((IDisposable)streamReader2).Dispose();
							}
						}
						if (((HttpWebResponse)ex.Response).ContentType.Contains("json"))
						{
							result = JsonConvert.DeserializeObject<ResponseType>(text);
							return result;
						}
					}
					catch (Exception arg_135_0)
					{
						Log.WriteException(arg_135_0, "GetResponse second pass failed.");
					}
					result = responseType;
					return result;
				}
				Stream expr_14D = response.GetResponseStream();
				streamReader = new StreamReader(expr_14D);
				text = streamReader.ReadToEnd();
				streamReader.Close();
				expr_14D.Close();
				response.Close();
				result = JsonConvert.DeserializeObject<ResponseType>(text);
			}
			catch (Exception arg_180_0)
			{
				Log.WriteException(arg_180_0, "GetResponse failed crafting/commencing request.");
				result = Activator.CreateInstance<ResponseType>();
			}
			return result;
		}

		// Token: 0x040000CC RID: 204
		private static string _clientToken;
	}
}
