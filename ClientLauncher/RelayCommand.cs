﻿using System;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace ClientLauncher
{
	// Token: 0x0200000D RID: 13
	public class RelayCommand : ICommand
	{
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600004A RID: 74 RVA: 0x00002F54 File Offset: 0x00001154
		// (remove) Token: 0x0600004B RID: 75 RVA: 0x00002F8C File Offset: 0x0000118C
		[method: CompilerGenerated]
		[CompilerGenerated]
		private event EventHandler CanExecuteChangedInternal;

		// Token: 0x0600004C RID: 76 RVA: 0x00002FC1 File Offset: 0x000011C1
		public RelayCommand(Action<object> execute) : this(execute, new Predicate<object>(RelayCommand.DefaultCanExecute))
		{
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002FD6 File Offset: 0x000011D6
		public RelayCommand(Action<object> execute, Predicate<object> canExecute)
		{
			this._execute = execute;
			this._canExecute = canExecute;
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x0600004E RID: 78 RVA: 0x00002FEC File Offset: 0x000011EC
		// (remove) Token: 0x0600004F RID: 79 RVA: 0x00002FFB File Offset: 0x000011FB
		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
				this.CanExecuteChangedInternal += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
				this.CanExecuteChangedInternal -= value;
			}
		}

		// Token: 0x06000050 RID: 80 RVA: 0x0000300A File Offset: 0x0000120A
		public bool CanExecute(object parameter)
		{
			return this._canExecute != null && this._canExecute(parameter);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003022 File Offset: 0x00001222
		public void Execute(object parameter)
		{
			if (this._execute != null)
			{
				this._execute(parameter);
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00003038 File Offset: 0x00001238
		public void OnCanExecuteChanged()
		{
			EventHandler canExecuteChangedInternal = this.CanExecuteChangedInternal;
			if (canExecuteChangedInternal != null)
			{
				canExecuteChangedInternal(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000053 RID: 83 RVA: 0x0000305B File Offset: 0x0000125B
		public void Destroy()
		{
			this._canExecute = null;
			this._execute = null;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x0000306B File Offset: 0x0000126B
		private static bool DefaultCanExecute(object parameter)
		{
			return true;
		}

		// Token: 0x04000025 RID: 37
		private Action<object> _execute;

		// Token: 0x04000026 RID: 38
		private Predicate<object> _canExecute;
	}
}
