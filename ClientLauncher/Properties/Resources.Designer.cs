﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ClientLauncher.Properties
{
	// Token: 0x02000034 RID: 52
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class Resources
	{
		// Token: 0x060001BF RID: 447 RVA: 0x00002344 File Offset: 0x00000544
		internal Resources()
		{
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x000076BB File Offset: 0x000058BB
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (Resources.resourceMan == null)
				{
					Resources.resourceMan = new ResourceManager("ClientLauncher.Properties.Resources", typeof(Resources).Assembly);
				}
				return Resources.resourceMan;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x000076E7 File Offset: 0x000058E7
		// (set) Token: 0x060001C2 RID: 450 RVA: 0x000076EE File Offset: 0x000058EE
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		// Token: 0x040000C9 RID: 201
		private static ResourceManager resourceMan;

		// Token: 0x040000CA RID: 202
		private static CultureInfo resourceCulture;
	}
}
