﻿using System;

namespace ClientLauncher
{
	// Token: 0x0200001A RID: 26
	internal enum OvertakePages
	{
		// Token: 0x0400004A RID: 74
		None,
		// Token: 0x0400004B RID: 75
		AccountSelector,
		// Token: 0x0400004C RID: 76
		AccountLogin
	}
}
