﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ClientLauncher.Library
{
	// Token: 0x0200004C RID: 76
	internal class LibraryManager
	{
		// Token: 0x0600020F RID: 527 RVA: 0x00008510 File Offset: 0x00006710
		public List<JsonLibrary> GetForVersion(Downloader downloader, string version, JObject dependenciesJsonObject)
		{
			List<JsonLibrary> list = new List<JsonLibrary>();
			using (IEnumerator<JObject> enumerator = ((JArray)dependenciesJsonObject.SelectToken("libraries")).Children<JObject>().GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					JsonLibrary jsonLibrary = JsonConvert.DeserializeObject<JsonLibrary>(enumerator.Current.ToString());
					if (jsonLibrary.IsValid())
					{
						string localPath = jsonLibrary.GetLocalPath(version);
						if (!Utility.FileExists(localPath))
						{
							Log.Write(string.Format("Missing file {0}, will need to download.", localPath));
							jsonLibrary.DownloadFile(version);
						}
						else
						{
							string text = FileHash.sha1(localPath).ToLower();
							string text2 = jsonLibrary.GetArtifact().sha1.ToLower();
							FileHash.md5(localPath).ToLower();
							if (text != text2)
							{
								Log.Write(string.Format("Hash of {0} ({1}) does not match expected hash {2}, will need to download.", localPath, text, text2));
								jsonLibrary.DownloadFile(version);
								string text3 = FileHash.sha1(localPath).ToLower();
								if (text3 != text2)
								{
									Log.Write(string.Format("New hash of {0} ({1}) STILL does not match expected hash {2}!", localPath, text3, text2));
								}
								else
								{
									Log.Write("New hash matches.");
								}
							}
						}
						list.Add(jsonLibrary);
					}
				}
			}
			list.RemoveAll((JsonLibrary item) => !Utility.FileExists(item.GetLocalPath(version)));
			foreach (JsonLibrary current in list)
			{
				if (current.IsNative())
				{
					try
					{
						string nativesDir = CheatBreakerGlobals.GetNativesDir(version);
						Utility.TryCreateDirectory(nativesDir);
						using (FileStream fileStream = new FileStream(current.GetLocalPath(version), FileMode.Open))
						{
							using (ZipArchive zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read))
							{
								zipArchive.ExtractToDirectory(nativesDir, true);
							}
						}
					}
					catch (Exception arg_1F9_0)
					{
						string localPath2 = current.GetLocalPath(version);
						Log.WriteException(arg_1F9_0, ("Failed to write file from zip: " + localPath2) ?? string.Empty);
					}
				}
			}
			if (Utility.DirectoryExists(CheatBreakerGlobals.GetVersionDir(version) + "natives\\META-INF\\"))
			{
				Utility.TryDirectoryDelete(CheatBreakerGlobals.GetVersionDir(version) + "natives\\META-INF\\", true);
			}
			return list;
		}
	}
}
