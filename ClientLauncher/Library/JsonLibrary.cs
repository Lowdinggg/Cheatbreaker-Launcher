﻿using System;
using System.Net;

namespace ClientLauncher.Library
{
	// Token: 0x02000048 RID: 72
	public class JsonLibrary
	{
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x000082A8 File Offset: 0x000064A8
		// (set) Token: 0x060001F1 RID: 497 RVA: 0x000082B0 File Offset: 0x000064B0
		public LibraryDownloads downloads
		{
			get;
			set;
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x000082B9 File Offset: 0x000064B9
		public bool IsValid()
		{
			return this.downloads != null && this.GetArtifact() != null;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x000082CE File Offset: 0x000064CE
		public bool IsNative()
		{
			return this.downloads.classifiers != null;
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x000082DE File Offset: 0x000064DE
		public string GetLocalPath(string version)
		{
			if (this.downloads == null)
			{
				return null;
			}
			return CheatBreakerGlobals.GetLibraryDir(version) + this.GetArtifact().path.Replace("/", "\\");
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00008310 File Offset: 0x00006510
		public LibraryArtifact GetArtifact()
		{
			if (this.downloads == null)
			{
				return null;
			}
			if (this.downloads.artifact != null)
			{
				return this.downloads.artifact;
			}
			if (this.downloads.classifiers != null)
			{
				if (this.downloads.classifiers.artifactUniversal != null)
				{
					return this.downloads.classifiers.artifactUniversal;
				}
				if (this.downloads.classifiers.artifact64 != null)
				{
					return this.downloads.classifiers.artifact64;
				}
			}
			return null;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x00008394 File Offset: 0x00006594
		private void SimpleDownloadFile(string urlSource, string targetFile)
		{
			using (WebClient webClient = new WebClient())
			{
				try
				{
					Log.Write(string.Format("Downloading library file from {0}", urlSource));
					webClient.DownloadFile(new Uri(urlSource), targetFile);
					Log.Write("Download complete.");
				}
				catch (Exception expr_2F)
				{
					Log.WriteException(expr_2F, "Failed to download library file from: " + urlSource + " for " + targetFile);
					throw expr_2F;
				}
			}
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x00008410 File Offset: 0x00006610
		public bool DownloadFile(string version)
		{
			if (this.downloads == null)
			{
				return false;
			}
			try
			{
				Utility.TryCreateDirectory(Utility.DirectoryGetParent(this.GetLocalPath(version)));
				this.SimpleDownloadFile(this.GetArtifact().url, this.GetLocalPath(version));
			}
			catch (Exception arg_3B_0)
			{
				Log.WriteException(arg_3B_0, "Failed download library file.");
				return false;
			}
			return true;
		}
	}
}
