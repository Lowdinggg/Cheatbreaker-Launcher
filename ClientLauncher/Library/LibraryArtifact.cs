﻿using System;

namespace ClientLauncher.Library
{
	// Token: 0x0200004A RID: 74
	public class LibraryArtifact
	{
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001FE RID: 510 RVA: 0x00008496 File Offset: 0x00006696
		// (set) Token: 0x060001FF RID: 511 RVA: 0x0000849E File Offset: 0x0000669E
		public int size
		{
			get;
			set;
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000200 RID: 512 RVA: 0x000084A7 File Offset: 0x000066A7
		// (set) Token: 0x06000201 RID: 513 RVA: 0x000084AF File Offset: 0x000066AF
		public string sha1
		{
			get;
			set;
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000202 RID: 514 RVA: 0x000084B8 File Offset: 0x000066B8
		// (set) Token: 0x06000203 RID: 515 RVA: 0x000084C0 File Offset: 0x000066C0
		public string path
		{
			get;
			set;
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000204 RID: 516 RVA: 0x000084C9 File Offset: 0x000066C9
		// (set) Token: 0x06000205 RID: 517 RVA: 0x000084D1 File Offset: 0x000066D1
		public string url
		{
			get;
			set;
		}
	}
}
