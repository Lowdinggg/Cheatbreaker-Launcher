﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace ClientLauncher
{
	// Token: 0x02000004 RID: 4
	internal class Sha1
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000022EC File Offset: 0x000004EC
		public static string GetSha1Hash(string filePath)
		{
			string result;
			using (FileStream fileStream = File.OpenRead(filePath))
			{
				result = BitConverter.ToString(new SHA1Managed().ComputeHash(fileStream)).ToLower().Replace("-", "");
			}
			return result;
		}
	}
}
